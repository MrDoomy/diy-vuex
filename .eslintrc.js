const WARN = 1;
const OFF = 0;

module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/vue3-essential', 'eslint:recommended', '@vue/prettier'],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? WARN : OFF,
    'no-debugger': process.env.NODE_ENV === 'production' ? WARN : OFF,
    'prettier/prettier': [
      WARN,
      {
        printWidth: 120,
        singleQuote: true,
        trailingComma: 'none',
        arrowParens: 'avoid'
      }
    ]
  }
};
